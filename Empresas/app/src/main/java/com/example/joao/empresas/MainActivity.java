package com.example.joao.empresas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private static EditText username;
    private static EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoginButton();
    }

    public void LoginButton() {

        username = (EditText)findViewById(R.id.editText1);
        password = (EditText)findViewById(R.id.editText2);

    }

    public void login(View view) {
        if (username.getText().toString().equals("testeapple@ioasys.com.br") && password.getText().toString().equals("12341234")) {

            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
        }
    }

}